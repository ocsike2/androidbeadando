package com.beres_tibor.androidbeadando;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
//import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ListView listViewResult;
    private EditText searchBar;
    private Button searchButton;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listViewResult = findViewById(R.id.list_view_result);
        searchBar = findViewById(R.id.searchbar);
        searchButton = findViewById(R.id.search_button);
        //textViewResult = findViewById(R.id.text_view_result);
        final ArrayList<String> listElements = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<List<Post>> call = jsonPlaceHolderApi.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (!response.isSuccessful()) {
                    listElements.add("Code: " + response.code());
                    return;
                }

                List<Post> posts = response.body();

                for (Post post : posts) {

                    String line = "";
                    line += "ID: " + post.getId() + ", ";
                    line += "Title: " + post.getTitle() + ", ";
                    line += "Text: " + post.getText();

                    listElements.add(line);
                }

                writeList(listElements);
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                listElements.add(t.getMessage());
            }


        });

        searchButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id=searchBar.getText().toString()+",";

                if (id.length()==1){
                    writeList(listElements);
                } else {
                    List<String> foundedItem=new ArrayList<>();

                    for (String item: listElements) {
                        String helper[]=item.split(" ");
                        System.out.println("Helper "+helper[0]);
                        System.out.println("Helper1 "+helper[1]);
                        System.out.println("Helper2 "+helper[2]);

                        if(id.equals(helper[1])){
                            foundedItem.add(item);
                        }

                    }
                    writeList(foundedItem);
                }
            }
        });


    }


    public void writeList(List<String> listElements) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listElements);
        listViewResult.setAdapter(arrayAdapter);
        listViewResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String clickedItem = (String) listViewResult.getItemAtPosition(position);
                Toast.makeText(MainActivity.this, clickedItem, Toast.LENGTH_LONG).show();
            }
        });
    }
}
